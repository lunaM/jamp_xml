package com.epam.jamp.xml.models;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;


@XmlRootElement(name = "book")
@XmlAccessorType(XmlAccessType.FIELD)
public class Book {
	
	@XmlAttribute
	private String id;
	@XmlElement(name = "author")
	private String author;
	@XmlElement(name = "title")
	private String title;
	@XmlElement(name = "genre")
	private String genre;
	@XmlElement(name = "price")
	private Double price;
	@XmlSchemaType(name = "date")
	@XmlElement(name = "publish_date")
	private Date publishDate;
	@XmlElement(name = "description")
	private String description;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGenre() {
		return genre;
	}
	
	public void setGenre(String genre) {
		this.genre = genre;
	}

	public Double getPrice() {
		return price;
	}
	
	public void setPrice(Double price) {
		this.price = price;
	}

	public Date getPublishDate() {
		return publishDate;
	}

	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	@Override
	public String toString() {
		return "Book id " + id + "\n\t author: " + author + "\n\t title: " + title + "\n\t genre: " + genre
				+ "\n\t price: " + price + "\n\t publishDate: " + publishDate + "\n\t description: " + description
				+ "\n";
	}

}
