package com.epam.jamp.xml.parsers;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.epam.jamp.xml.models.Book;
import com.epam.jamp.xml.models.BookCatalog;


public class SAXParserHandler extends DefaultHandler {

	private String characters;
	private BookCatalog catalog = new BookCatalog();
	private List<Book> books = null;
	private Book book = null;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		if (qName.equalsIgnoreCase("booksList")) {
			books = new ArrayList<>();
			catalog.setBooks(books);
		} else if (qName.equalsIgnoreCase("book")) {
			book = new Book();
			book.setId(attributes.getValue("id"));
			books.add(book);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("author")) {
			book.setAuthor(characters);
		}else if(qName.equalsIgnoreCase("title")){
			if(book != null){
				book.setTitle(characters);
			}else{
				catalog.setTitle(characters);
			}
		}else if(qName.equalsIgnoreCase("genre")){
			book.setGenre(characters);
		}else if(qName.equalsIgnoreCase("price")){
			book.setPrice(Double.valueOf(characters));
		}else if(qName.equalsIgnoreCase("publish_date")){
			try {
				book.setPublishDate(formatter.parse(characters));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}else if(qName.equalsIgnoreCase("description")){
			book.setDescription(characters);
		}else if(qName.equalsIgnoreCase("book")){
			books.add(book);
			book = null;
		}
	}

	public void characters(char ch[], int start, int length) throws SAXException {
		characters = new String(ch, start, length);
	}
	
	public static BookCatalog parseBookCatalog(String filePath) throws ParserConfigurationException, SAXException, IOException{
		SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = factory.newSAXParser();
        SAXParserHandler userhandler = new SAXParserHandler();
        saxParser.parse(new File(filePath), userhandler);
        return userhandler.getCatalog();
	}
	
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		BookCatalog catalog = SAXParserHandler.parseBookCatalog("D:\\vik\\java\\Mentorship Program 2\\workspace\\XmlParsers\\src\\main\\resources\\XML_TASK.xml");
		System.out.println(catalog);
	}
	
	public BookCatalog getCatalog() {
		return catalog;
	}

	public void setCatalog(BookCatalog catalog) {
		this.catalog = catalog;
	}
}
