package com.epam.jamp.xml.parsers;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.epam.jamp.xml.models.BookCatalog;

public class JaxbBookCatalog {

	public static void marshaling(BookCatalog catalog, String path) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(BookCatalog.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(catalog, new File(path));
	}

	public static BookCatalog unMarshaling(String path) throws JAXBException {
		JAXBContext jaxbContext = JAXBContext.newInstance(BookCatalog.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		BookCatalog catalog = (BookCatalog) jaxbUnmarshaller.unmarshal(new File(path));
		return catalog;
	}
	
	public static void main(String[] args) throws JAXBException {
		BookCatalog catalog = JaxbBookCatalog.unMarshaling("D:\\vik\\java\\Mentorship Program 2\\workspace\\XmlParsers\\src\\main\\resources\\XML_TASK.xml");
		System.out.println(catalog);
		JaxbBookCatalog.marshaling(catalog, "D:\\vik\\java\\Mentorship Program 2\\workspace\\XmlParsers\\src\\main\\resources\\result_catalog.xml");
	}
}
