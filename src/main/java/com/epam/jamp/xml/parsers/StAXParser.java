package com.epam.jamp.xml.parsers;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import com.epam.jamp.xml.models.Book;
import com.epam.jamp.xml.models.BookCatalog;

public class StAXParser {

	private XMLInputFactory factory = XMLInputFactory.newInstance();

	public BookCatalog parse(String filePath) throws FileNotFoundException, XMLStreamException {
		XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(filePath));
		BookCatalog catalog = new BookCatalog();
		List<Book> books = null;
		Book book = null;
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String currentElement = null;
		while (eventReader.hasNext()) {
			XMLEvent event = eventReader.nextEvent();
			switch (event.getEventType()) {
			case XMLStreamConstants.START_ELEMENT:
				StartElement startElement = event.asStartElement();
				String qName = startElement.getName().getLocalPart();
				currentElement = qName;
				if (qName.equalsIgnoreCase("booksList")) {
					books = new ArrayList<>();
					catalog.setBooks(books);
				} else if (qName.equalsIgnoreCase("book")) {
					book = new Book();
					book.setId(startElement.getAttributeByName(new QName("id")).getValue());
					books.add(book);
				}
				break;
			case XMLStreamConstants.CHARACTERS:
				Characters chars = event.asCharacters();
				String characters = chars.getData();
				if (currentElement.equalsIgnoreCase("author")) {
					book.setAuthor(characters);
				} else if (currentElement.equalsIgnoreCase("title")) {
					if (book != null && book.getTitle() == null) {
						book.setTitle(characters);
					} else if(catalog.getTitle() == null){
						catalog.setTitle(characters);
					}
				} else if (currentElement.equalsIgnoreCase("genre")) {
					book.setGenre(characters);
				} else if (currentElement.equalsIgnoreCase("price")) {
					book.setPrice(Double.valueOf(characters));
				} else if (currentElement.equalsIgnoreCase("publish_date")) {
					try {
						book.setPublishDate(formatter.parse(characters));
					} catch (ParseException e) {
						e.printStackTrace();
					}
				} 
				else if (currentElement.equalsIgnoreCase("description")) {
					book.setDescription(characters);
				}
				currentElement="";
				break;
			case XMLStreamConstants.END_ELEMENT:
				EndElement endElement = event.asEndElement();
				String eName = endElement.getName().getLocalPart();
				if (eName.equalsIgnoreCase("book")) {
					books.add(book);
					book = null;
				}
				break;
			}
		}
		return catalog;
	}

	public static void main(String[] args) throws FileNotFoundException, XMLStreamException {
		System.out.println(new StAXParser().parse(
				"D:\\vik\\java\\Mentorship Program 2\\workspace\\XmlParsers\\src\\main\\resources\\XML_TASK.xml"));
	}
}
