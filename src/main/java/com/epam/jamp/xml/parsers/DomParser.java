package com.epam.jamp.xml.parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xquery.XQConnection;
import javax.xml.xquery.XQDataSource;
import javax.xml.xquery.XQException;
import javax.xml.xquery.XQPreparedExpression;
import javax.xml.xquery.XQResultSequence;

import org.codehaus.staxmate.dom.DOMConverter;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.epam.jamp.xml.models.Book;
import com.epam.jamp.xml.models.BookCatalog;

import net.sf.saxon.xqj.SaxonXQDataSource;

public class DomParser {

	private DocumentBuilderFactory factory = null;
	private DocumentBuilder builder = null;
	private Document doc = null;
	private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

	public DomParser() {
		factory = DocumentBuilderFactory.newInstance();
		try {
			builder = factory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
	}

	public BookCatalog parse(String pathToFile) throws SAXException, IOException, DOMException, ParseException {
		doc = builder.parse(new File(pathToFile));
		doc.getDocumentElement().normalize();
		BookCatalog catalog = new BookCatalog();
		NodeList title = doc.getElementsByTagName("title");
		if (title.getLength() != 0) {
			catalog.setTitle(title.item(0).getTextContent());
		}
		NodeList bookNodes = doc.getElementsByTagName("book");
		catalog.setBooks(parseBooks(bookNodes));
		return catalog;
	}

	protected List<Book> parseBooks(NodeList bookNodes) throws DOMException, ParseException {
		List<Book> bookList = null;
		if (bookNodes.getLength() != 0) {
			bookList = new ArrayList<>();
		}
		for (int i = 0; i < bookNodes.getLength(); i++) {
			Node node = bookNodes.item(i);
			bookList.add(parseBook(node));
		}
		return bookList;
	}
	
	public Book parseBook(Node node) throws DOMException, ParseException{
		Book book = new Book();
		if (node.getNodeType() == Node.ELEMENT_NODE) {
			Element bookElement = (Element) node;
			book.setId(bookElement.getAttribute("id"));
			NodeList params = bookElement.getChildNodes();
			for (int j = 0; j < params.getLength(); j++) {
				Node param = params.item(j);
				if (param.getNodeType() == Node.ELEMENT_NODE) {
					switch (param.getNodeName()) {
					case "author":
						book.setAuthor(param.getTextContent());
						break;
					case "title":
						book.setTitle(param.getTextContent());
						break;
					case "genre":
						book.setGenre(param.getTextContent());
						break;
					case "price":
						if (param.getTextContent() != null) {
							book.setPrice(Double.valueOf(param.getTextContent()));
						}
						break;
					case "publish_date":
						if (param.getTextContent() != null) {
							book.setPublishDate(formatter.parse(param.getTextContent()));
						}
						break;
					case "description":
						book.setDescription(param.getTextContent());
						break;
					}
				}
			}
		}
		return book;
	}

	public List<Book> allBooksWhereAuthorIs(String pathToFile, String author)
			throws SAXException, IOException, XPathExpressionException, DOMException, ParseException {
		List<Book> books = null;
		String expression = "//book[author[contains(text()," + author + ")]]";
		doc = builder.parse(new File(pathToFile));
		doc.getDocumentElement().normalize();
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
		books = parseBooks(nodeList);
		return books;
	}

	public List<Book> findAllBooks(String pathXQuery)
			throws FileNotFoundException, XQException, XMLStreamException, DOMException, ParseException {
		InputStream inputStream = new FileInputStream(new File(pathXQuery));
		XQDataSource ds = new SaxonXQDataSource();
		XQConnection conn = ds.getConnection();
		XQPreparedExpression exp = conn.prepareExpression(inputStream);
		XQResultSequence result = exp.executeQuery();
		List<Book> books = new ArrayList<>();
		while (result.next()) {
			doc = new DOMConverter().buildDocument(result.getItemAsStream());
			doc.getDocumentElement().normalize();
			books.add(parseBook(doc.getDocumentElement()));
	     }
		return books;
	}

	public static void main(String[] args) throws Exception {
		DomParser parser = new DomParser();
		String path = "D:\\vik\\java\\Mentorship Program 2\\workspace\\XmlParsers\\src\\main\\resources\\";
		System.out.println(parser.parse(path + "XML_TASK.xml"));
		System.out.println("\n\nBooks with author \"O'Brien\"");
		for (Book book : parser.allBooksWhereAuthorIs(path+ "XML_TASK.xml", "\"O'Brien\"")) {
			System.out.println(book);
		}
		 System.out.println("\n\nXQuery to find all books with price bigger then 5.95");
		 System.out.println(parser.findAllBooks(path+"books.xqy"));
	}
}
